Name:           armadillo
Version:        14.2.1
Release:        1
Summary:        Fast C++ matrix library with syntax similar to MATLAB and Octave
License:        Apache-2.0
URL:            http://arma.sourceforge.net/
Source:         http://sourceforge.net/projects/arma/files/%{name}-%{version}.tar.xz

BuildRequires:  cmake lapack-devel arpack-devel hdf5-devel openblas-devel SuperLU-devel gcc-g++


%description
Armadillo is a C ++ linear algebra library and is an important choice.

%package devel
Summary:        Development headers and documentation for the Armadillo C++ library
Requires:       %{name} = %{version}-%{release}
Requires:       lapack-devel arpack-devel libstdc++-devel hdf5-devel openblas-devel SuperLU-devel

%description devel
This package contains header files.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

sed -i 's/\r//' README.md

%build
%cmake
%cmake_build

%install
%cmake_install

rm -f examples/{Makefile.cmake,example1_win64.sln,example1_win64.vcxproj,example1_win64.README.txt}
rm -rf examples/example2_win64.*
rm -rf examples/lib_win64

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%{_libdir}/*.so.*
%license LICENSE.txt NOTICE.txt

%files devel
%{_libdir}/*.so
%{_libdir}/pkgconfig/%{name}.pc
%{_includedir}/armadillo
%{_includedir}/armadillo_bits/
%{_datadir}/Armadillo/

%files help
%doc README.md index.html docs.html mex_interface
%doc examples armadillo_icon.png
%doc armadillo_*.pdf

%changelog
* Thu Nov 28 2024 yaoxin <yao_xin001@hoperun.com> - 14.2.1-1
- Update to 14.2.1:
  * faster handling of symmetric matrices by inv(), rcond(), powmat()
  * faster handling of hermitian matrices by inv(), rcond(), powmat(), cond(), pinv(), rank()
  * added solve_opts::force_sym option to solve() to force use of the symmetric/hermitian solver (not limited to sympd matrices)
  * more efficient handling of compound expressions by solve()

* Wed Oct 30 2024 xuezhixin <xuezhixin@uniontech.com> - 12.6.2-3
- use the new cmake macros

* Mon Aug 19 2024 Pan Zhang <zhangpan@cqsoftware.com.cn> - 12.6.2-2
- Replaced  declaration of help subpackage with the 'package_help' macro.

* Thu Sep 21 2023 wulei <wu_lei@hoperun.com> - 12.6.2-1
- Update to 12.6.2

* Fri Jan 21 2022 SimpleUpdate Robot <tc@openeuler.org> - 10.7.4-1
- Upgrade to version 10.7.4

* Mon May 31 2021 baizhonggui <baizhonggui@huawei.com> - 9.600.6-5
- Add gcc-g++ in BuildRequires

* Fri 26 Mar 2021 sunguoshuai <sunguoshuai@huawei.com> - 9.600.6-4
- Delete depends on atlas

* Thu Mar 5 2020 wangye <wangye54@huawei.com> - 9.600.6-3
- Update 

* Thu Mar 5 2020 wangye <wangye54@huawei.com> - 9.600.6-2
- Package init
